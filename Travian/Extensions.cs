﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Travian
{
    public static class Extensions
    {
        public static IEnumerable<HtmlElement> ElementsByClass(this HtmlDocument doc, string className)
        {
            foreach (HtmlElement e in doc.All)
                if (e.GetAttribute("className") == className)
                    yield return e;
        }

        public static IEnumerable<HtmlElement> ElementsByName(this HtmlDocument doc, string className)
        {
            foreach (HtmlElement e in doc.All)
                if (e.GetAttribute("name") == className)
                    yield return e;
        }

        public static IEnumerable<HtmlElement> ElementsByName(this HtmlElement doc, string className)
        {
            foreach (HtmlElement e in doc.All)
                if (e.GetAttribute("name") == className)
                    yield return e;
        }

        public static IEnumerable<HtmlElement> ElementsByClass(this HtmlElement doc, string className)
        {
            foreach (HtmlElement e in doc.All)
                if (e.GetAttribute("className") == className)
                    yield return e;
        }
    }
}
