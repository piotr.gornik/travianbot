﻿namespace Travian
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button3 = new System.Windows.Forms.Button();
            this.messageTo = new System.Windows.Forms.TextBox();
            this.messageSubject = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.messageText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.addOrUpgradeBuilding = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.IsVisibleRadioButton = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.attackY = new System.Windows.Forms.NumericUpDown();
            this.attackX = new System.Windows.Forms.NumericUpDown();
            this.atackPlunder = new System.Windows.Forms.RadioButton();
            this.atakNormal = new System.Windows.Forms.RadioButton();
            this.Reinforcements = new System.Windows.Forms.RadioButton();
            this.Attackdestination = new System.Windows.Forms.TextBox();
            this.t11 = new System.Windows.Forms.NumericUpDown();
            this.t10 = new System.Windows.Forms.NumericUpDown();
            this.t8 = new System.Windows.Forms.NumericUpDown();
            this.t7 = new System.Windows.Forms.NumericUpDown();
            this.t5 = new System.Windows.Forms.NumericUpDown();
            this.t6 = new System.Windows.Forms.NumericUpDown();
            this.t9 = new System.Windows.Forms.NumericUpDown();
            this.t4 = new System.Windows.Forms.NumericUpDown();
            this.t3 = new System.Windows.Forms.NumericUpDown();
            this.t2 = new System.Windows.Forms.NumericUpDown();
            this.t1 = new System.Windows.Forms.NumericUpDown();
            this.r1 = new System.Windows.Forms.NumericUpDown();
            this.r2 = new System.Windows.Forms.NumericUpDown();
            this.r3 = new System.Windows.Forms.NumericUpDown();
            this.r4 = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.makeNewWarior = new System.Windows.Forms.NumericUpDown();
            this.button6 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.attackY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.r4)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.makeNewWarior)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Run browser";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(2, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Buy Fields";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(84, 34);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Send Message To:";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // messageTo
            // 
            this.messageTo.Location = new System.Drawing.Point(134, 19);
            this.messageTo.Name = "messageTo";
            this.messageTo.Size = new System.Drawing.Size(235, 20);
            this.messageTo.TabIndex = 5;
            // 
            // messageSubject
            // 
            this.messageSubject.Location = new System.Drawing.Point(55, 48);
            this.messageSubject.Name = "messageSubject";
            this.messageSubject.Size = new System.Drawing.Size(314, 20);
            this.messageSubject.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.messageText);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.messageSubject);
            this.groupBox1.Controls.Add(this.messageTo);
            this.groupBox1.Location = new System.Drawing.Point(12, 443);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(376, 161);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Messages";
            // 
            // messageText
            // 
            this.messageText.Location = new System.Drawing.Point(9, 74);
            this.messageText.Multiline = true;
            this.messageText.Name = "messageText";
            this.messageText.Size = new System.Drawing.Size(360, 80);
            this.messageText.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Subject";
            // 
            // addOrUpgradeBuilding
            // 
            this.addOrUpgradeBuilding.Location = new System.Drawing.Point(2, 61);
            this.addOrUpgradeBuilding.Name = "addOrUpgradeBuilding";
            this.addOrUpgradeBuilding.Size = new System.Drawing.Size(138, 20);
            this.addOrUpgradeBuilding.TabIndex = 8;
            this.addOrUpgradeBuilding.Text = "Buy or upgrade building";
            this.addOrUpgradeBuilding.UseVisualStyleBackColor = true;
            this.addOrUpgradeBuilding.Click += new System.EventHandler(this.addOrUpgradeBuilding_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(146, 61);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(48, 20);
            this.numericUpDown2.TabIndex = 9;
            this.numericUpDown2.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(201, 61);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 10;
            // 
            // IsVisibleRadioButton
            // 
            this.IsVisibleRadioButton.AutoSize = true;
            this.IsVisibleRadioButton.Checked = true;
            this.IsVisibleRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IsVisibleRadioButton.Location = new System.Drawing.Point(84, 7);
            this.IsVisibleRadioButton.Name = "IsVisibleRadioButton";
            this.IsVisibleRadioButton.Size = new System.Drawing.Size(56, 17);
            this.IsVisibleRadioButton.TabIndex = 12;
            this.IsVisibleRadioButton.Text = "Visible";
            this.IsVisibleRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.attackY);
            this.groupBox2.Controls.Add(this.attackX);
            this.groupBox2.Controls.Add(this.atackPlunder);
            this.groupBox2.Controls.Add(this.atakNormal);
            this.groupBox2.Controls.Add(this.Reinforcements);
            this.groupBox2.Controls.Add(this.Attackdestination);
            this.groupBox2.Controls.Add(this.t11);
            this.groupBox2.Controls.Add(this.t10);
            this.groupBox2.Controls.Add(this.t8);
            this.groupBox2.Controls.Add(this.t7);
            this.groupBox2.Controls.Add(this.t5);
            this.groupBox2.Controls.Add(this.t6);
            this.groupBox2.Controls.Add(this.t9);
            this.groupBox2.Controls.Add(this.t4);
            this.groupBox2.Controls.Add(this.t3);
            this.groupBox2.Controls.Add(this.t2);
            this.groupBox2.Controls.Add(this.t1);
            this.groupBox2.Location = new System.Drawing.Point(12, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(376, 251);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Send Military";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(272, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Bohater";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(272, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Osadnicy";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(272, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Herszt";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(137, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Taran";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(137, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Haeduan";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(137, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Jeździec druidzki";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(137, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Grom Teutatesa";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(248, 191);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Atakuj!";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(137, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Y:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "X:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Wioska";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Grom Teutatesa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Tropiciel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Miecznik";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Falangita";
            // 
            // attackY
            // 
            this.attackY.Location = new System.Drawing.Point(164, 147);
            this.attackY.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.attackY.Minimum = new decimal(new int[] {
            9999,
            0,
            0,
            -2147483648});
            this.attackY.Name = "attackY";
            this.attackY.Size = new System.Drawing.Size(48, 20);
            this.attackY.TabIndex = 0;
            // 
            // attackX
            // 
            this.attackX.Location = new System.Drawing.Point(83, 147);
            this.attackX.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.attackX.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
            this.attackX.Name = "attackX";
            this.attackX.Size = new System.Drawing.Size(48, 20);
            this.attackX.TabIndex = 0;
            // 
            // atackPlunder
            // 
            this.atackPlunder.AutoSize = true;
            this.atackPlunder.Location = new System.Drawing.Point(243, 168);
            this.atackPlunder.Name = "atackPlunder";
            this.atackPlunder.Size = new System.Drawing.Size(95, 17);
            this.atackPlunder.TabIndex = 17;
            this.atackPlunder.Text = "Atack: Plunder";
            this.atackPlunder.UseVisualStyleBackColor = true;
            // 
            // atakNormal
            // 
            this.atakNormal.AutoSize = true;
            this.atakNormal.Location = new System.Drawing.Point(243, 145);
            this.atakNormal.Name = "atakNormal";
            this.atakNormal.Size = new System.Drawing.Size(92, 17);
            this.atakNormal.TabIndex = 16;
            this.atakNormal.Text = "Atack: Normal";
            this.atakNormal.UseVisualStyleBackColor = true;
            // 
            // Reinforcements
            // 
            this.Reinforcements.AutoSize = true;
            this.Reinforcements.Checked = true;
            this.Reinforcements.Location = new System.Drawing.Point(243, 122);
            this.Reinforcements.Name = "Reinforcements";
            this.Reinforcements.Size = new System.Drawing.Size(99, 17);
            this.Reinforcements.TabIndex = 15;
            this.Reinforcements.TabStop = true;
            this.Reinforcements.Text = "Reinforcements";
            this.Reinforcements.UseVisualStyleBackColor = true;
            // 
            // Attackdestination
            // 
            this.Attackdestination.Location = new System.Drawing.Point(55, 121);
            this.Attackdestination.Name = "Attackdestination";
            this.Attackdestination.Size = new System.Drawing.Size(157, 20);
            this.Attackdestination.TabIndex = 9;
            // 
            // t11
            // 
            this.t11.Location = new System.Drawing.Point(329, 69);
            this.t11.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t11.Name = "t11";
            this.t11.Size = new System.Drawing.Size(40, 20);
            this.t11.TabIndex = 14;
            // 
            // t10
            // 
            this.t10.Location = new System.Drawing.Point(329, 43);
            this.t10.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t10.Name = "t10";
            this.t10.Size = new System.Drawing.Size(40, 20);
            this.t10.TabIndex = 13;
            // 
            // t8
            // 
            this.t8.Location = new System.Drawing.Point(226, 95);
            this.t8.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t8.Name = "t8";
            this.t8.Size = new System.Drawing.Size(40, 20);
            this.t8.TabIndex = 12;
            // 
            // t7
            // 
            this.t7.Location = new System.Drawing.Point(226, 69);
            this.t7.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t7.Name = "t7";
            this.t7.Size = new System.Drawing.Size(40, 20);
            this.t7.TabIndex = 11;
            // 
            // t5
            // 
            this.t5.Location = new System.Drawing.Point(226, 19);
            this.t5.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t5.Name = "t5";
            this.t5.Size = new System.Drawing.Size(40, 20);
            this.t5.TabIndex = 10;
            // 
            // t6
            // 
            this.t6.Location = new System.Drawing.Point(226, 43);
            this.t6.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t6.Name = "t6";
            this.t6.Size = new System.Drawing.Size(40, 20);
            this.t6.TabIndex = 9;
            // 
            // t9
            // 
            this.t9.Location = new System.Drawing.Point(329, 17);
            this.t9.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t9.Name = "t9";
            this.t9.Size = new System.Drawing.Size(40, 20);
            this.t9.TabIndex = 8;
            // 
            // t4
            // 
            this.t4.Location = new System.Drawing.Point(91, 95);
            this.t4.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.t4.Name = "t4";
            this.t4.Size = new System.Drawing.Size(40, 20);
            this.t4.TabIndex = 7;
            // 
            // t3
            // 
            this.t3.Location = new System.Drawing.Point(91, 69);
            this.t3.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.t3.Name = "t3";
            this.t3.Size = new System.Drawing.Size(40, 20);
            this.t3.TabIndex = 6;
            // 
            // t2
            // 
            this.t2.Location = new System.Drawing.Point(91, 43);
            this.t2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.t2.Name = "t2";
            this.t2.Size = new System.Drawing.Size(40, 20);
            this.t2.TabIndex = 5;
            // 
            // t1
            // 
            this.t1.Location = new System.Drawing.Point(91, 19);
            this.t1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.t1.Name = "t1";
            this.t1.Size = new System.Drawing.Size(40, 20);
            this.t1.TabIndex = 4;
            // 
            // r1
            // 
            this.r1.Location = new System.Drawing.Point(56, 19);
            this.r1.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.r1.Name = "r1";
            this.r1.Size = new System.Drawing.Size(40, 20);
            this.r1.TabIndex = 13;
            // 
            // r2
            // 
            this.r2.Location = new System.Drawing.Point(55, 45);
            this.r2.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.r2.Name = "r2";
            this.r2.Size = new System.Drawing.Size(40, 20);
            this.r2.TabIndex = 14;
            // 
            // r3
            // 
            this.r3.Location = new System.Drawing.Point(149, 19);
            this.r3.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.r3.Name = "r3";
            this.r3.Size = new System.Drawing.Size(40, 20);
            this.r3.TabIndex = 15;
            // 
            // r4
            // 
            this.r4.Location = new System.Drawing.Point(149, 45);
            this.r4.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.r4.Name = "r4";
            this.r4.Size = new System.Drawing.Size(40, 20);
            this.r4.TabIndex = 16;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(231, 220);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(111, 23);
            this.button5.TabIndex = 28;
            this.button5.Text = "Wyślij surowce";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.r1);
            this.groupBox3.Controls.Add(this.r2);
            this.groupBox3.Controls.Add(this.r4);
            this.groupBox3.Controls.Add(this.r3);
            this.groupBox3.Location = new System.Drawing.Point(9, 170);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 73);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Surowce";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Drewno";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 47);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "Glina";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(103, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "Żelazo";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(106, 47);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Zboże";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Controls.Add(this.makeNewWarior);
            this.groupBox4.Location = new System.Drawing.Point(12, 345);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(376, 92);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // makeNewWarior
            // 
            this.makeNewWarior.Location = new System.Drawing.Point(64, 25);
            this.makeNewWarior.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.makeNewWarior.Name = "makeNewWarior";
            this.makeNewWarior.Size = new System.Drawing.Size(40, 20);
            this.makeNewWarior.TabIndex = 14;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(18, 63);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 23);
            this.button6.TabIndex = 29;
            this.button6.Text = "Wyszkol";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Falangita";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 616);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.IsVisibleRadioButton);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.addOrUpgradeBuilding);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.attackY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.r4)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.makeNewWarior)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox messageTo;
        private System.Windows.Forms.TextBox messageSubject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox messageText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addOrUpgradeBuilding;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox IsVisibleRadioButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown attackY;
        private System.Windows.Forms.NumericUpDown attackX;
        private System.Windows.Forms.RadioButton atackPlunder;
        private System.Windows.Forms.RadioButton atakNormal;
        private System.Windows.Forms.RadioButton Reinforcements;
        private System.Windows.Forms.TextBox Attackdestination;
        private System.Windows.Forms.NumericUpDown t11;
        private System.Windows.Forms.NumericUpDown t10;
        private System.Windows.Forms.NumericUpDown t8;
        private System.Windows.Forms.NumericUpDown t7;
        private System.Windows.Forms.NumericUpDown t5;
        private System.Windows.Forms.NumericUpDown t6;
        private System.Windows.Forms.NumericUpDown t9;
        private System.Windows.Forms.NumericUpDown t4;
        private System.Windows.Forms.NumericUpDown t3;
        private System.Windows.Forms.NumericUpDown t2;
        private System.Windows.Forms.NumericUpDown t1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown r1;
        private System.Windows.Forms.NumericUpDown r2;
        private System.Windows.Forms.NumericUpDown r3;
        private System.Windows.Forms.NumericUpDown r4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.NumericUpDown makeNewWarior;
    }
}

