using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Travian
{
    public static class TravianLinks
    {
        public static string MainPage = "http://ts6.travian.pl/dorf2.php";
        public static string SendMessage = "https://ts6.travian.pl/messages.php?t=1";
        public static string BuyFields = "https://ts6.travian.pl/build.php?id=";
        public static string MakeWariors = "https://ts6.travian.pl/build.php?id=32";
        public static string SendAttack = "https://ts6.travian.pl/build.php?id=39&tt=2";
        public static string SendItems = "https://ts6.travian.pl/build.php?t=5&id=35";
        public static string SendItemsAjax = "https://ts6.travian.pl/ajax.php?cmd=prepareMarketplace";
        public static string CheckTime = "https://ts6.travian.pl/dorf1.php";
        public static void PrapareLinks(int villageID, int baracksID, int marketID)
        {
        }
    }
    public enum Command
    {
        Login,
        Navigate,
        CheckTimer,
        BuyBuilding,
        Dispose,
        SendAttack,
        AfterAttack,
        SendItems,
        PrepareSendItems,
        MakeWariors,
    }

    public enum TaskType
    {
        BuyBuilding,
        BuyField,
    }

    public enum Buildings
    {
        Wybierz = 0,



        Tartak = 5,
        Cegielnia = 6,
        HutaStali = 7,
        Mlyn = 8,
        Piekarnia = 9,
        Magazyn = 10,
        Spichlerz = 11,

        WarsztatPlatnerza = 13,
        PlacTurniejowy = 14,
        GlownyBudynek = 15,
        MiejsceZbiorki = 16,
        Rynek = 17,
        Ambasada = 18,
        Koszary = 19,
        Stajnia = 20,
        Warsztat = 21,
        Akademia = 22,
        Kryjowka = 23,
        Ratusz = 24,
        Rezydencja = 25,

        Skarbiec = 27,
        Targ = 28,


        Palisada = 33,
        Kamieniarz = 34,
        ChataTrapera = 36,
        DworBochatera = 37,
        DuzyMagazyn = 38,
        DuzySpichlerz = 39,
    }

    public class Attack
    {
        public string Destination { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public int AttackType { get; set; }
        public List<decimal> Military { get; set; }
    }

    public class IEBrowser : ApplicationContext
    {
        #region props
        AutoResetEvent resultEvent;
        Command CurrentCommand;
        string userName, password;

        int navigationCounter;
        ScriptCallback scriptCallback;
        Uri UrlBeforeTimeCheck;
        string urlHelper;
        Attack attackObject;
        decimal WariorCount;

        WebBrowser ieBrowser;
        Form form;

        public Command CommandBeforeTimeCheck { get; private set; }
        public string AjaxToken { get; private set; }

        public IEBrowser()
        {
        }
        #endregion

        public IEBrowser(bool visible, string userName, string password, AutoResetEvent resultEvent)
        {
            this.userName = "turbobai";
            this.password = "password";
            this.resultEvent = resultEvent;
        }

        public void Init(bool visible)
        {
            scriptCallback = new ScriptCallback(this);

            ieBrowser = new WebBrowser();
            ieBrowser.ScriptErrorsSuppressed = true;
            ieBrowser.ObjectForScripting = scriptCallback;
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_Login);
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_SendAttack);
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_SendItems);
            ieBrowser.Navigating += new WebBrowserNavigatingEventHandler(IEBrowser_Navigating);

            if (visible)
            {
                form = new Form();
                ieBrowser.Dock = DockStyle.Fill;
                form.Controls.Add(ieBrowser);
                form.Visible = visible;
                form.Size = new System.Drawing.Size(800, 800);
            }

            navigationCounter = 0;
            ieBrowser.Navigate(TravianLinks.MainPage);
            CurrentCommand = Command.Login;
        }

        internal void SendMessage(string text1, string text2, string text3)
        {
            string postData = "an=" + text1 + "&be=" + text2 + "&message=" + text3;
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            byte[] bytes = encoding.GetBytes(postData);
            var loginData = new NameValueCollection
            {
                { "name", "turbobai" },
                { "password", "password" },
                {"s1","Login" },
                {"w", "1920:1080" },
                {"login" , (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString() }
            };
            var client = new CookieAwareWebClient();
            client.Login(TravianLinks.SendMessage, loginData);
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            string HtmlResult = client.UploadString(TravianLinks.SendMessage, postData);
            client.Dispose();

            CurrentCommand = Command.Dispose;
            // ieBrowser.Navigate(url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");
        }

        internal void MakeWariors(decimal value)
        {
            WariorCount = value;
            CurrentCommand = Command.MakeWariors;
            ieBrowser.Navigate(TravianLinks.MakeWariors);
        }

        internal void SendAttack(Attack attack)
        {
            attackObject = attack;
            CurrentCommand = Command.SendAttack;
            ieBrowser.Navigate(TravianLinks.SendAttack);
        }

        internal void SendItems(Attack attack)
        {
            attackObject = attack;
            CurrentCommand = Command.PrepareSendItems;
            ieBrowser.Navigate(TravianLinks.SendItems);
        }

        internal void BuyField(int id)
        {
            scriptCallback = new ScriptCallback(this);

            ieBrowser = new WebBrowser();
            ieBrowser.ScriptErrorsSuppressed = true;
            ieBrowser.ObjectForScripting = scriptCallback;
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_Commons);
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_BuyFieldsOrBuildings);
            ieBrowser.Navigating += new WebBrowserNavigatingEventHandler(IEBrowser_Navigating);

            navigationCounter = 0;
            CurrentCommand = Command.BuyBuilding;
            ieBrowser.Navigate(TravianLinks.BuyFields + id);
        }

        internal void BuyBuildiing(int id, Buildings builingId)
        {
            scriptCallback = new ScriptCallback(this);

            ieBrowser = new WebBrowser();
            ieBrowser.ScriptErrorsSuppressed = true;
            ieBrowser.ObjectForScripting = scriptCallback;
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_Commons);
            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted_BuyFieldsOrBuildings);
            ieBrowser.Navigating += new WebBrowserNavigatingEventHandler(IEBrowser_Navigating);

            navigationCounter = 0;
            CurrentCommand = Command.BuyBuilding;
            urlHelper = $"https://ts6.travian.pl/dorf2.php?a={(int)builingId}&id={id}&c=";//TODO trzeba to przemyśleć
            ieBrowser.Navigate(TravianLinks.BuyFields + id);
        }

        void IEBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            navigationCounter++;
            if (form != null) form.Text = e.Url.ToString();
        }

        void IEBrowser_DocumentCompleted_Login(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.Equals(ieBrowser.Url))
            {
                if (CurrentCommand == Command.Login)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    var isLoginPage = (doc.GetElementById("content")?.InnerHtml.Contains("Zaloguj się"));
                    if (doc.Title.Equals("Travian pl6") && isLoginPage.HasValue ? isLoginPage.Value : false)
                    {
                        doc.GetElementById("name").SetAttribute("value", userName);
                        doc.GetElementById("password").SetAttribute("value", password);
                        doc.GetElementById("s1").InvokeMember("click");
                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                    CurrentCommand = Command.Navigate;
                }
                if (CurrentCommand == Command.MakeWariors && e.Url.Equals(ieBrowser.Url) && e.Url.ToString().Contains(TravianLinks.MakeWariors))
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    var isLoginPage = (doc.GetElementById("content")?.InnerHtml.Contains("Koszary"));
                    if (doc.Title.Equals("Travian pl6") && isLoginPage.HasValue ? isLoginPage.Value : false)
                    {
                        doc.ElementsByName("t1").FirstOrDefault()?.SetAttribute("value", WariorCount.ToString());
                        doc.GetElementById("s1").InvokeMember("click");
                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                    CurrentCommand = Command.Navigate;
                }
            }
        }

        void IEBrowser_DocumentCompleted_BuyFieldsOrBuildings(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.Equals(ieBrowser.Url) && e.Url.ToString().Contains("https://ts6.travian.pl/build.php"))
            {
                if (CurrentCommand == Command.BuyBuilding)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    var isbuildPage = (doc.GetElementById("content")?.InnerHtml.Contains("Poziom"));
                    if (doc.Title.Equals("Travian pl6") && isbuildPage.HasValue ? isbuildPage.Value : false)
                    {
                        var builing = doc.ElementsByClass("buildingWrapper").FirstOrDefault();
                        HtmlElement button = null;
                        if (builing != null) // To na pewno jesteśmy w trybie budowy nowego budunku
                            button = builing.ElementsByClass("green new").FirstOrDefault();
                        else
                            button = doc.ElementsByClass("contentContainer").FirstOrDefault().ElementsByClass("green build").FirstOrDefault();

                        if (button == null)
                        {
                            var error = doc.ElementsByClass("statusMessage").FirstOrDefault();
                            if (error != null)
                            {
                                var value = error.Children[0].Children[0].GetAttribute("value");
                                int time;
                                if (int.TryParse(value, out time))
                                    Thread.Sleep(time * 1000);
                                else
                                    Thread.Sleep(60000);
                                ieBrowser.Navigate(((WebBrowser)sender).Url);
                                CurrentCommand = Command.Dispose;
                                return;
                            }
                            UrlBeforeTimeCheck = ((WebBrowser)sender).Url; // sets Url for future callback
                            CommandBeforeTimeCheck = CurrentCommand;
                            CheckTimer(); // Sends to main site chcecks timer and sets it
                            return;
                        }
                        else
                        {
                            if (builing != null) // To na pewno jesteśmy w trybie budowy nowego budunku
                            {
                                int iStartPos = button.OuterHtml.IndexOf("onclick=\"") + ("onclick=\"").Length;
                                int iEndPos = button.OuterHtml.IndexOf("\" ", iStartPos);

                                var onclick = button.OuterHtml.Substring(iStartPos, iEndPos - iStartPos).Split('&').FirstOrDefault(x => x.Contains("c="));
                                var indexOfEqual = onclick.IndexOf('=') + 1;
                                var indexOfQuote = onclick.IndexOf('\'');
                                var c = onclick.Substring(indexOfEqual, indexOfQuote - indexOfEqual);
                                CurrentCommand = Command.Dispose;
                                ieBrowser.Navigate(urlHelper + c, string.Empty, null, "Content-Type: application/x-www-form-urlencoded");
                                return;
                            }
                            else
                            {
                                button.InvokeMember("click");
                                Application.ExitThread();
                                return;
                            }
                        }
                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                }
            }
        }

        void IEBrowser_DocumentCompleted_SendAttack(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.Equals(ieBrowser.Url) && e.Url.ToString().Contains("https://ts6.travian.pl/build.php?id=39&tt=2"))
            {
                if (CurrentCommand == Command.SendAttack)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    if (doc.Title.Equals("Travian pl6"))
                    {
                        var error = doc.ElementsByClass("error").FirstOrDefault();
                        if (error != null)
                        {
                            MessageBox.Show(error.InnerHtml);
                        }
                        else
                        {
                            var timestamp = doc.ElementsByName("timestamp").FirstOrDefault().GetAttribute("value");
                            var b = doc.ElementsByName("b").FirstOrDefault().GetAttribute("value");
                            var timestamp_cheksum = doc.ElementsByName("timestamp_checksum").FirstOrDefault().GetAttribute("value");
                            var currentDid = doc.ElementsByName("currentDid").FirstOrDefault().GetAttribute("value");


                            string postData = $"timestamp={timestamp}&timestamp_checksum={timestamp_cheksum}&b={b}&currentDid={currentDid}&c={attackObject.AttackType}&s1=ok";
                            if (string.IsNullOrEmpty(attackObject.Destination))
                                postData += $"&x={attackObject.X}&y={attackObject.Y}&dname=";
                            else
                                postData += $"&x=&y=&dname={attackObject.Destination}";
                            for (int i = 0; i < attackObject.Military.Count; i++)
                            {
                                if (attackObject.Military[i] == 0)
                                    postData += $"&t{i + 1}=";
                                else
                                    postData += $"t{i + 1}={attackObject.Military[i]}";
                            }
                            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                            byte[] bytes = encoding.GetBytes(postData);
                            var url = "https://ts6.travian.pl/build.php?id=39&tt=2";
                            CurrentCommand = Command.AfterAttack;
                            ieBrowser.Navigate(url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");
                        }

                        return;

                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                }
                if (CurrentCommand == Command.AfterAttack)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    if (doc.Title.Equals("Travian pl6"))
                    {
                        var error = doc.ElementsByClass("error").FirstOrDefault();
                        if (error != null)
                        {
                            MessageBox.Show(error.InnerHtml);
                        }
                        return;
                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                }
            }
        }

        void IEBrowser_DocumentCompleted_SendItems(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.Equals(ieBrowser.Url) && e.Url.ToString().Contains(TravianLinks.SendItems))
            {
                if (CurrentCommand == Command.PrepareSendItems)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    if (doc.Title.Equals("Travian pl6"))
                    {
                        var data = new NameValueCollection();
                        var error = doc.ElementsByClass("error").FirstOrDefault();
                        if (error != null && !string.IsNullOrEmpty(error.InnerHtml))
                        {
                            MessageBox.Show(error.InnerHtml);
                        }
                        else
                        {
                            #region oldbackup
                            //var loginData = new NameValueCollection
                            //{
                            //    { "name", "turbobai" },
                            //    { "password", "password" },
                            //    {"s1","Login" },
                            //    {"w", "1920:1080" },
                            //    {"login" , (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString() }
                            //};
                            //var client = new CookieAwareWebClient();
                            //client.Login("https://ts6.travian.pl/build.php?t=5&id=35", loginData);
                            //client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                            //string HtmlResult22 = client.UploadString("https://ts6.travian.pl/build.php?t=5&id=35", string.Empty);
                            #endregion
                            var x2 = 1;
                            var someHtml = doc.GetElementsByTagName("HEAD")[0].InnerHtml;
                            var startIndex = someHtml.IndexOf("window.ajaxToken = '") + ("window.ajaxToken = '").Length;
                            int endIndex = someHtml.IndexOf("';", startIndex);

                            var ajaxToken = someHtml.Substring(startIndex, endIndex - startIndex).Replace(";", string.Empty).Replace("'", string.Empty);

                            string postData = $"x2={x2}&id={35}&t={5}&cmd=prepareMarketplace";


                            if (string.IsNullOrEmpty(attackObject.Destination))
                                postData += $"&x={attackObject.X}&y={attackObject.Y}&dname=";
                            else
                                postData += $"&x=&y=&dname={attackObject.Destination}";

                            for (int i = 0; i < attackObject.Military.Count; i++)
                            {
                                if (attackObject.Military[i] == 0)
                                    postData += $"&r{i + 1}=";
                                else
                                    postData += $"&r{i + 1}={attackObject.Military[i]}";
                            }
                            postData += $"&ajaxToken={ajaxToken}";
                            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                            byte[] bytes = encoding.GetBytes(postData);
                            AjaxToken = ajaxToken;

                            //string HtmlResult = client.UploadString(url, postData);
                            //client.Dispose();
                            //MessageBox.Show(HtmlResult);

                            CurrentCommand = Command.SendItems;
                            ieBrowser.Navigate(TravianLinks.SendItemsAjax, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");



                        }

                        return;

                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                }
            }
            if (CurrentCommand == Command.SendItems)
            {
                HtmlDocument doc = ((WebBrowser)sender).Document;
                if (true)
                {
                    var error = doc?.ElementsByClass("error").FirstOrDefault();
                    if (error != null && !string.IsNullOrEmpty(error.InnerHtml))
                    {
                        MessageBox.Show(error.InnerHtml);
                    }
                    else
                    {
                        var destinationForm = doc?.ElementsByClass("destination").FirstOrDefault();
                        var x2 = 1;
                        if (destinationForm == null)
                        {
                            var someHtml = doc.GetElementsByTagName("HEAD")[0].InnerHtml;
                            var a = doc.ElementsByName("\\\"a\\\"").FirstOrDefault().GetAttribute("value").Replace("\\", string.Empty).Replace("\"", string.Empty);
                            var sz = doc.ElementsByName("\\\"sz\\\"").FirstOrDefault().GetAttribute("value").Replace("\\", string.Empty).Replace("\"", string.Empty);
                            var kid = doc.ElementsByName("\\\"kid\\\"").FirstOrDefault().GetAttribute("value").Replace("\\", string.Empty).Replace("\"", string.Empty);
                            var c = doc.ElementsByName("\\\"c\\\"").FirstOrDefault().GetAttribute("value").Replace("\\", string.Empty).Replace("\"", string.Empty);

                            //var startIndex = doc.Url.ToString().IndexOf("ajaxToken=") + ("ajaxToken=").Length;
                            //var ajaxToken = doc.Url.ToString().Substring(startIndex);

                            string postData = $"x2={x2}&ajaxToken={AjaxToken}&id={35}&t={5}&cmd=prepareMarketplace&a={a}&sz={sz}&kid={kid}&c={c}";

                            for (int i = 0; i < attackObject.Military.Count; i++)
                            {
                                if (attackObject.Military[i] == 0)
                                    postData += $"&r{i + 1 }=";
                                else
                                    postData += $"&r{i + 1}={attackObject.Military[i]}";
                            }

                            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                            byte[] bytes = encoding.GetBytes(postData);
                            CurrentCommand = Command.Dispose;
                            ieBrowser.Navigate(TravianLinks.SendItemsAjax, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");
                        }
                        else
                        {
                            doc.InvokeScript("eloElo");
                        }
                    }

                    return;
                }
            }
        }

        void IEBrowser_DocumentCompleted_Commons(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.Equals(ieBrowser.Url))
            {
                if (CurrentCommand == Command.CheckTimer)
                {
                    HtmlDocument doc = ((WebBrowser)sender).Document;
                    if (doc.Title.Equals("Travian pl6"))
                    {
                        var elements = doc.ElementsByClass("buildDuration");
                        if (elements.Any())
                        {
                            var element = elements.FirstOrDefault();
                            var value = element.Children[0].GetAttribute("value");

                            int time;
                            if (int.TryParse(value, out time))
                            {
                                Thread.Sleep(time * 1000 + 5000);
                            }
                            else
                            {
                                Thread.Sleep(60000);
                            }
                        }
                        ieBrowser.Navigate(UrlBeforeTimeCheck);
                        CurrentCommand = CommandBeforeTimeCheck;

                    }
                    else
                    {
                        doc.InvokeScript("setTimeout", new object[] { string.Format("window.external.getHtmlResult({0})", navigationCounter), 10 });
                    }
                }
                else if (CurrentCommand == Command.Navigate)
                {
                    return;
                }
                else if (CurrentCommand == Command.Dispose)
                {
                    Application.ExitThread();
                    return;
                }
            }
        }

        void CheckTimer()
        {
            CurrentCommand = Command.CheckTimer;
            ieBrowser.Navigate(TravianLinks.CheckTime);
        }

        protected override void Dispose(bool disposing)
        {
            //if (ieBrowser != null)
            //{
            //    System.Runtime.InteropServices.Marshal.Release(ieBrowser.Handle);
            //    ieBrowser.Dispose();
            //}
            //if (form != null) form.Dispose();
            base.Dispose(disposing);
            Application.ExitThread();
        }
    }
    [System.Runtime.InteropServices.ComVisible(true)]
    public class ScriptCallback
    {
        IEBrowser owner;
        static string scriptPattern;

        public ScriptCallback(IEBrowser owner)
        {
            this.owner = owner;

            // read JScript.js only once
            if (scriptPattern != null) return;
            StreamReader rd = File.OpenText(System.AppDomain.CurrentDomain.BaseDirectory + "JSScript.js");
            scriptPattern = rd.ReadToEnd();
            rd.Close();
        }
    }

    public class CookieAwareWebClient : WebClient
    {
        public void Login(string loginPageAddress, NameValueCollection loginData)
        {
            CookieContainer container;

            var request = (HttpWebRequest)WebRequest.Create(loginPageAddress);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            var query = string.Join("&",
              loginData.Cast<string>().Select(key => $"{key}={loginData[key]}"));

            var buffer = Encoding.ASCII.GetBytes(query);
            request.ContentLength = buffer.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);
            requestStream.Close();

            container = request.CookieContainer = new CookieContainer();

            var response = request.GetResponse();
            response.Close();
            CookieContainer = container;
        }

        public CookieAwareWebClient(CookieContainer container)
        {
            CookieContainer = container;
        }

        public CookieAwareWebClient()
          : this(new CookieContainer())
        { }

        public CookieContainer CookieContainer { get; private set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            return request;
        }
    }
}
