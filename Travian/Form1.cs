﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travian
{
    public partial class Form1 : Form
    {
        IEBrowser browser;
        private Thread browserThread;
        PRLFlashback TaskQueuee;
        Task taskA;

        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var visible = IsVisibleRadioButton.Checked;
            var resultEvent = new AutoResetEvent(false);
            browser = new IEBrowser(true, "turbobai", "password", resultEvent);


            browserThread = new Thread(new ThreadStart(() => { browser.Init(visible); Application.Run(browser); }));
            browserThread.IsBackground = true;
            browserThread.SetApartmentState(ApartmentState.STA);
            browserThread.Start();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            #region old
            // IEBrowser backgroundbrowser = new IEBrowser();
            // var thrd2 = new Thread(new ThreadStart(() =>
            //{
            //    backgroundbrowser.BuyField((int)numericUpDown1.Value);
            //    Application.Run(backgroundbrowser);
            //}));

            // thrd2.SetApartmentState(ApartmentState.STA);
            // thrd2.Start();

            // thrd2.Join();
            #endregion
            TaskQueuee.AddTask(TaskType.BuyField, new List<int>() { (int)numericUpDown1.Value });
        }

        private void button3_Click(object sender, EventArgs e)
        {
            browser.SendMessage(messageTo.Text, messageSubject.Text, messageText.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = Enum.GetValues(typeof(Buildings));
            TaskQueuee = new PRLFlashback();
            taskA = Task.Run(() => TaskQueuee.Work());
            this.FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void addOrUpgradeBuilding_Click(object sender, EventArgs e)
        {
            var builingType = (int)comboBox1.SelectedItem;
            TaskQueuee.AddTask(TaskType.BuyBuilding, new List<int>() { (int)numericUpDown1.Value, builingType });
            #region old
            // IEBrowser backgroundbrowser = new IEBrowser();


            // var thrd2 = new Thread(new ThreadStart(() =>
            //{
            //    backgroundbrowser.BuyBuildiing((int)numericUpDown2.Value, builingType);
            //    Application.Run(backgroundbrowser);
            //}));

            // thrd2.SetApartmentState(ApartmentState.STA);
            // thrd2.Start();

            //TaskQueuee.AddTask(TaskType.BuyBuilding, new List<int>() { (int)numericUpDown2.Value, (int)builingType });
            #endregion
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var attackType = Reinforcements.Checked ? 2 : atakNormal.Checked ? 3 : 4;
            var military = new List<decimal>() { t1.Value, t2.Value, t3.Value, t4.Value, t5.Value, t6.Value, t7.Value, t8.Value, t9.Value, t10.Value, t11.Value };

            var attack = new Attack()
            {
                AttackType = attackType,
                Destination = Attackdestination.Text,
                X = attackX.Value,
                Y = attackY.Value,
                Military = military
            };
            browser.SendAttack(attack);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var attackType = Reinforcements.Checked ? 2 : atakNormal.Checked ? 3 : 4;
            var military = new List<decimal>() { r1.Value, r2.Value, r3.Value, r4.Value, };

            var attack = new Attack()
            {
                AttackType = attackType,
                Destination = Attackdestination.Text,
                X = attackX.Value,
                Y = attackY.Value,
                Military = military
            };
            browser.SendItems(attack);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            browser.MakeWariors(makeNewWarior.Value);
        }
    }

    public class PRLFlashback
    {
        private ConcurrentQueue<TrevianTask> Tasks;
        IEBrowser backgroundbrowser;
        delegate void CalculateThreadDelegate();

        public PRLFlashback()
        {
            Tasks = new ConcurrentQueue<TrevianTask>();
            backgroundbrowser = new IEBrowser();
        }

        public void AddTask(TaskType tasktype, List<int> args)
        {
            Tasks.Enqueue(new TrevianTask(tasktype, args));
        }

        public void Work()
        {
            CalculateThreadDelegate calcPointer = null;
            while (true)
            {
                if (Tasks.Any())
                {
                    TrevianTask task = null;
                    while (!Tasks.TryDequeue(out task)) ;

                    switch (task.WorkType)
                    {
                        case TaskType.BuyBuilding:
                            calcPointer = () => { backgroundbrowser.BuyBuildiing(task.Args[0], (Buildings)task.Args[1]); Application.Run(backgroundbrowser); };
                            break;

                        case TaskType.BuyField:
                            calcPointer = () => { backgroundbrowser.BuyField(task.Args[0]); Application.Run(backgroundbrowser); };
                            break;
                        default:
                            break;
                    }

                    var browserThread = new Thread(new ThreadStart(calcPointer));
                    browserThread.SetApartmentState(ApartmentState.STA);
                    browserThread.Start();
                    browserThread.Join();
                }
            }
        }

        private class TrevianTask
        {
            public TaskType WorkType { get; set; }
            public List<int> Args { get; set; }

            public TrevianTask(TaskType workType, List<int> args)
            {
                WorkType = workType;
                Args = args;
            }
        }
    }
}
